﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Mvc;
using VideoChat.Models;

namespace VideoChat.Controllers
{
    public class HomeController : Controller
    {
        private Data data = new Data(); 

        public ActionResult Index()
        {
            try
            {
                var cook = Request.Cookies["login"];
                var keyCook = Request.Cookies["key"];
                var passKook = Request.Cookies["pass"];

                if (cook != null && keyCook != null && passKook != null && !String.IsNullOrEmpty(keyCook.Value))
                {
                    ViewBag.Login = HttpUtility.UrlDecode(cook.Value);
                    ViewBag.MyKey = keyCook.Value;
                    ViewBag.Password = passKook.Value;
                }
              
                var model = data.GetUsersStore().GetChatHistory(C.MAIN_CHAT_GROUP).OrderBy(m => m.Date).ToList();

                return View(model);
            }
            catch
            {
                return View("Error");
            }
        }

        [System.Web.Http.HttpGet]
        public ActionResult GetChatHistory(string roomKey = C.MAIN_CHAT_GROUP)
        {
            var model = data.GetUsersStore().GetChatHistory(roomKey).OrderBy(m => m.Date).ToList();
           
            return Json(model);
        }
        [System.Web.Http.HttpPost]
        public ActionResult UpdateRoomState(string roomKey, VideoState state)
        {
            try
            {
                var keyCook = Request.Cookies["key"];

                if (keyCook != null && !String.IsNullOrEmpty(keyCook.Value))
                {
                    data.GetUsersStore().CloseRoom(roomKey, keyCook.Value, state);
                }

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false });
            }
        }

        [System.Web.Http.HttpPost]
        public ActionResult AddFriend(string myKey, string friendKey, string myName, string friendName)
        {
            var keyCook = Request.Cookies["key"];
            try
            {
                if (keyCook != null && !String.IsNullOrEmpty(keyCook.Value))
                {
                    data.GetUsersStore().AddFriend(myKey, friendKey, myName, friendName);
                }

                return Json(new { success = true });
            }
            catch (Exception rexp)
            {
                return Json(new { success = false });
            }
        }

        [System.Web.Http.HttpPost]
        public ActionResult RemoveFriend(string myKey, string friendKey)
        {
            var keyCook = Request.Cookies["key"];
            try
            {
                if (keyCook != null && !String.IsNullOrEmpty(keyCook.Value))
                {
                    data.GetUsersStore().RemoveFriend(myKey, friendKey);
                }

                return Json(new { success = true });
            }
            catch (Exception rexp)
            {
                return Json(new { success = false });
            }
        }

        [System.Web.Http.HttpPost]
        public ActionResult CheckIsFriend(string myKey, string friendKey)
        {
            var keyCook = Request.Cookies["key"];
            try
            {
                var result = false;

                if (keyCook != null && !String.IsNullOrEmpty(keyCook.Value))
                {
                    result = data.GetUsersStore().CheckIsFriend(myKey, friendKey);
                }

                return Json(new { success = result });
            }
            catch (Exception rexp)
            {
                return Json(new { success = false });
            }
        }

        [HttpPost]
        public JsonResult Login(string login, string password, string passwordconfirm)
        {
            var data = new Data();
            var errors = new Dictionary<string, string>();

            if (!String.IsNullOrEmpty(login) && !String.IsNullOrEmpty(password) && login.Length < 255)
            {
                if ((String.IsNullOrEmpty(passwordconfirm) && data.GetUsersStore().NameExists(login) && data.GetUsersStore().UserExists(login, password)) || (!String.IsNullOrEmpty(passwordconfirm) && passwordconfirm == password && !data.GetUsersStore().NameExists(login)))
                {
                    var keyCook = Request.Cookies["key"];
                    var passCook = Request.Cookies["pass"];
                    var roleCook = Request.Cookies["role"];

                    var userPermissions = data.GetUsersStore().GetRoleForCred(login, password);
                    if (userPermissions != null && userPermissions.Contains("Admin"))
                    {
                        roleCook = new HttpCookie("role", HttpUtility.UrlEncode("Admin"));
                        HttpContext.Response.Cookies.Add(roleCook);
                    }
                    else if (userPermissions != null && userPermissions.Contains("Moderator"))
                    {
                        roleCook = new HttpCookie("role", HttpUtility.UrlEncode("Moderaror"));
                        HttpContext.Response.Cookies.Add(roleCook);
                    }
                    else
                    {
                        roleCook = new HttpCookie("role", HttpUtility.UrlEncode("User"));
                        HttpContext.Response.Cookies.Add(roleCook);
                    }

                    if (keyCook == null && !String.IsNullOrEmpty(passwordconfirm))
                    {
                        keyCook = new HttpCookie("key", Guid.NewGuid().ToString());
                        HttpContext.Response.Cookies.Add(keyCook);
                    }
                    else
                    {
                        keyCook = new HttpCookie("key", data.GetUsersStore().GetKeyForExistUser(login, password));

                        HttpContext.Response.Cookies.Add(keyCook);
                    }
                    if (passCook == null)
                    {
                        passCook = new HttpCookie("pass", password);
                        HttpContext.Response.Cookies.Add(passCook);
                    }
                    var loginCook = new HttpCookie("login", HttpUtility.UrlEncode(login)) { Expires = DateTime.Now.AddDays(1) };
                    HttpContext.Response.Cookies.Add(loginCook);

                    return Json(new { success = true, name = login, password = password });
                }
                else
                {

                    if (!String.IsNullOrEmpty(passwordconfirm) && data.GetUsersStore().NameExists(login))
                    {
                        errors.Add("login", "Это имя занято");
                    }
                    else
                    {
                        errors.Add("login", "Введите корректную информацию");
                    }
                }

                return Json(new { success = false, errors });
            }
            return Json(new { success = false, errors });
        }

        public ActionResult LogOut()
        {

            var keyCook = new HttpCookie("key", null);
            var passCook = new HttpCookie("pass", null);
            var loginCook = new HttpCookie("login", null);
            var roleCook = new HttpCookie("role", null);
            HttpContext.Response.Cookies.Add(keyCook);
            HttpContext.Response.Cookies.Add(passCook);
            HttpContext.Response.Cookies.Add(loginCook);
            HttpContext.Response.Cookies.Add(roleCook);

            return RedirectToAction("Index", "Home");
        }


        [System.Web.Http.HttpGet]
        public ActionResult Room(bool isMy, string roomKey)
        {
            try
            {
                var model = data.GetUsersStore().GetRoomInfo(roomKey);

                if (!isMy)
                {
                    model = new RoomModel
                                {
                                    RoomKey = model.RoomKey,
                                    MyKey = model.CalleeKey,
                                    MyName = model.CalleeName,
                                    MyPublicKey = model.CalleePublicKey,
                                    CalleeKey = model.MyKey,
                                    CalleeName = model.MyName,
                                    CalleePublicKey = model.MyPublicKey
                                };
                }

                return View(model);
            }
            catch
            {
                return View("Error");
            }
        }

        [System.Web.Http.HttpPost]
        public ActionResult CloseRoom(string roomKey)
        {
            data.GetUsersStore().CloseRoom(roomKey);

            return Json(new { success = true });
        }

        public ActionResult SaveState(string talkId, string userKey, bool isCamera, bool isCameraOn, bool isMicrophone, bool isMicrophoneOn)
        {
            data.GetUsersStore().SaveRoomState(talkId, userKey, isCamera, isCameraOn, isMicrophone, isMicrophoneOn);
            return Content("OK");
        }
    }
}
