﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoChat.Models;

namespace VideoChat.Controllers
{
    public class TrendsController : Controller
    {
        public ActionResult Index()
        {
            try
            {

                var cook = Request.Cookies["login"];
                var keyCook = Request.Cookies["key"];
                var passKook = Request.Cookies["pass"];
                if (cook != null && keyCook != null && passKook != null && !String.IsNullOrEmpty(keyCook.Value))
                {
                    ViewBag.Login = HttpUtility.UrlDecode(cook.Value);
                    ViewBag.MyKey = keyCook.Value;
                    ViewBag.Password = passKook.Value;
                }

                var data = new Data();
                var model = data.GetUsersStore().GetTrendList().ToList();

                return View(model);
            }
            catch
            {
                return View("Error");
            }
        }
    }
}
