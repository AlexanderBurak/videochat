﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoChat.Managers;
using VideoChat.Models;
using PagedList;
using VideoChat.Providers;

namespace VideoChat.Controllers
{
    [AuthorizeUser(Roles = "Admin")]
    public class AdminController : Controller
    {
        private Data data = new Data();

        public ActionResult Index(int? page)
        {
            try
            {
                int pageNumber = (page ?? 1);
                int pageSize = 10;
                var model = data.GetUsersStore().GetAllUserList().ToPagedList(pageNumber, pageSize);

                return View(model);
            }
            catch
            {
                return View("Error");
            }

        }


        public ActionResult Details(string id)
        {
            try
            {
                string IP = data.GetUsersStore().GetUsersIp(id);

                if (IP == "::1")
                {
                    IP = System.Configuration.ConfigurationManager.AppSettings["CurrentIP"];
                }

                string coordinates = "";
                GeoManager geomanager = new GeoManager();
                ViewBag.Location = geomanager.DefineLocation(IP, ref coordinates, Server.MapPath("~/GeoBD/geobaza.dat"));
                ViewBag.Coords = coordinates;
                var model = data.GetUsersStore().GetUserInfForAdmin(id);

                return View(model);
            }
            catch
            {
                return View("Error");
            }
        }


        public ActionResult Edit(string id)
        {
            try
            {
                var model = data.GetUsersStore().GetUserInfForAdmin(id);

                return View(model);
            }
            catch (Exception exp)
            {
                return View("Error");
            }

        }


        [HttpPost]
        public ActionResult Edit(string id, User model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.Key = id;
                    data.GetUsersStore().EditUserProfile(model);
                    return RedirectToAction("Index");
                }
                else
                {
                    return View("Edit");
                }
            }
            catch
            {
                return View("Error");
            }
        }

        public ActionResult Delete(string id)
        {

            try
            {
                var model = data.GetUsersStore().GetUserInfForAdmin(id);

                return View(model);
            }
            catch (Exception exp)
            {
                return View("Error");
            }

            
        }


         [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(string id)
        {
            try
            {
                data.GetUsersStore().Remove(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}
