﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoChat.Managers;
using VideoChat.Models;
using ubs.GeoBazaAPI;

namespace VideoChat.Controllers
{
    public class GeoLocationController : Controller
    {
        private Data data = new Data(); 

        public ActionResult Index()
        {
            // получаем ip клиента (если не локальный хост)
            string IP = HttpContext.Request.UserHostAddress;

            if(IP=="::1")
            {
                IP = System.Configuration.ConfigurationManager.AppSettings["CurrentIP"];
            }

            // получаем географию
            string coordinates = "";
            GeoManager geomanager = new GeoManager();
            ViewBag.Location = geomanager.DefineLocation(IP, ref coordinates, Server.MapPath("~/GeoBD/geobaza.dat"));
            ViewBag.Coords = coordinates;

            return View();
        }

       
    }
}
