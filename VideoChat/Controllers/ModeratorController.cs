﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoChat.Models;
using PagedList;
using VideoChat.Providers;


namespace VideoChat.Controllers
{
    [AuthorizeUser(Roles = "Admin, Moderator")]
    public class ModeratorController : Controller
    {
        private Data data = new Data();

        public ActionResult Index(int? page)
        {
            try
            {                
                int pageSize = 10;
                int pageNumber = (page ?? 1);
                var data = new Data();
                var model = data.GetUsersStore().GetKeyWordList().ToList().ToPagedList(pageNumber, pageSize);
                
                return View(model);
            }

            catch (Exception exp)
            {
                return View("Error");
            }

        }


        public ActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception exp)
            {
                return View("Error");
            }

        }


        [HttpPost]
        public ActionResult Create(KeyWord model)
        {
            try
            {
                model.ModeratorKey = Request.Cookies["key"].Value;
                model.Key = data.GetUsersStore().GetMaxIdForkey()+1;
                data.GetUsersStore().AddKeyWord(model);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }


        public ActionResult Edit(int id)
        {
            try
            {
                var model = data.GetUsersStore().GetKeyById(id);

                return View(model);
            }
            catch (Exception exp)
            {
                return View("Error");
            }
        }

        public ActionResult Details(int id)
        {
            try
            {
                var model = data.GetUsersStore().GetKeyById(id);

                return View(model);
            }
            catch (Exception exp)
            {
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, KeyWord model)
        {
            try
            {
                model.Key = id;
                data.GetUsersStore().EditKeyWord(model);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }


        public ActionResult Delete(int id)
        {
            try
            {
                var model = data.GetUsersStore().GetKeyById(id);

                return View(model);
            }
            catch (Exception exp)
            {
                return View("Error");
            }
        }


        [HttpPost]
        public ActionResult Delete(int id, KeyWord model)
        {
            try
            {
                data.GetUsersStore().DeleteKeyWord(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
