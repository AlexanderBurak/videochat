﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using VideoChat.Models;


namespace VideoChat.Providers
{
    public class AuthorizeUserAttribute : AuthorizeAttribute
    {
        public string Roles { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var login = httpContext.Request.Cookies["login"].Value;
            var key = httpContext.Request.Cookies["key"].Value;

            if (login==null || key==null)
            {
                return false;
            }
            var data = new Data();
            var userPermissions = data.GetUsersStore().GetRoleForUser(key).Replace(" ", string.Empty);

            if (userPermissions != null && this.Roles.Contains(userPermissions))
            {
                return true;
            }


            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new
                            {
                                controller = "home",
                                action = "index"
                            })
                        );
        }

    }
}