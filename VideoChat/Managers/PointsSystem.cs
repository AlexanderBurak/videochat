﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using VideoChat.Models;

namespace VideoChat.Managers
{
    public class PointsSystem
    {
        static private Data data=new Data();

        public string CheckWord(string word, out string lang, out bool Changes)
        {
            lang = "?";
            string newword = word;
            string OnlyRu = "БбвГгДдЁёЖжЗзИиЙйЛлмнПптУФфЦцЧчШшЩщЪъЫыЬьЭэЮюЯя";
            string OnlyEn = "DdFfGghIiJjLlNQqRrSstUVvWwYZz";
            string Rus = "АаВбВвЕеКкМНОоРрСсТtуХхЗО1тиа@пьжэ";
            string Eng = "AaBbVvEeKkMHOoPpCcTtyXx30imu@anb*e";
            int rusCount = 0, engCount = 0;

            bool IsRu100percent = false;
            foreach (char c1 in word)
                foreach (char c2 in OnlyRu)
                    IsRu100percent = IsRu100percent || (c1 == c2);

            if (IsRu100percent)
            {
                lang = "ru";

                //Конвертируем все сомнительные символы в русские
                for (int i = 0; i < word.Length; i++)
                    if (Eng.IndexOf(word[i]) >= 0)
                    {
                        newword = newword.Replace(Eng[i], Rus[i]);
                    }
                for (int i = 0; i < Rus.Length; i++)
                    newword = newword.Replace(Eng[i], Rus[i]);
            }
            else
            {
                bool IsEn100percent = false;
                foreach (char c1 in word)
                    foreach (char c2 in OnlyEn)
                        IsEn100percent = IsEn100percent || (c1 == c2);
                if (IsEn100percent)
                {
                    lang = "en";
                    //Конвертируем все сомнительные символы в английские
                    for (int i = 0; i < word.Length; i++)
                    if (Eng.IndexOf(word[i]) >= 0)
                    {
                        newword = newword.Replace(Rus[i], Eng[i]);
                    }
                    for (int i = 0; i < Eng.Length; i++)
                        newword = newword.Replace(Rus[i], Eng[i]);

                }
            }
            //Были ли замены?
            Changes = newword != word;

            newword = newword.ToLower();
            
            return newword;
        }
        public bool CheckInDictionary(string content)
        {
            var list= data.GetUsersStore().GetKeyWordList();
            foreach (var keyword in list)
            {
                if(keyword.Name.ToLower().Contains(content.ToLower())||keyword.Description.ToLower().Contains(content.ToLower()))
                {
                    return true;
                }
            }

            return false;
        }

    }
}