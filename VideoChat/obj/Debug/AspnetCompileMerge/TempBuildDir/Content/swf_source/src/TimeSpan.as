package
{
	public class TimeSpan
	{
		private var totalSeconds:Number;
		private var totalMinutes:Number;
		private var totalHours:Number;
		private var totalDays:Number;
		
		private var seconds:Number;
		private var minutes:Number;
		private var hours:Number;
		private var days:Number;
		
		public function TimeSpan(diff:Number)
		{
			totalSeconds = Math.floor(diff / 1000);
			totalMinutes = Math.floor(totalSeconds / 60);
			totalHours = Math.floor(totalMinutes / 60);
			totalDays = Math.floor(totalHours / 24);
			
			days = Math.floor(totalDays);
			hours = Math.floor(totalHours - days*24);
			minutes = Math.floor(totalMinutes - totalHours*60);
			seconds = Math.floor(totalSeconds - totalMinutes*60);
		}
		
		public function getTotalSeconds():Number {return totalSeconds;}
		public function getTotalMinutes():Number {return totalMinutes;}
		public function getTotalHours():Number {return totalHours;}
		public function getTotalDays():Number {return totalDays;}
		
		public function getSeconds():Number {return seconds;}
		public function getMinutes():Number {return minutes;}
		public function getHours():Number {return hours;}
		public function getDays():Number {return days;}
	}
}