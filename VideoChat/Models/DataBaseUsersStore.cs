﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Dapper;

namespace VideoChat.Models
{
    public class DataBaseUsersStore : IUsersStore
    {
        private static readonly string ConnectionString =
              ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString;

        public static IDbConnection GetConnection()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            return connection;
        }

        public string GetKeyForExistUser(string name, string password)
        {
            using (var db = GetConnection())
            {
                return db.Query<string>("select UserKey from Account where  UserName=@name and Password=@password", new { name, password }).FirstOrDefault();
            }
        }

        public bool Exists(string key, string name, string password)
        {
            using (var db = GetConnection())
            {
                return db.Query<int>("select 1 from Account where UserKey = @key and UserName=@name and Password=@password", new { key, name, password }).FirstOrDefault() > 0;
            }
        }
        public bool UserExists(string name, string password)
        {
            using (var db = GetConnection())
            {
                return db.Query<int>("select 1 from Account where  UserName=@name and Password=@password", new { name, password }).FirstOrDefault() > 0;
            }
        }

        public bool CheckIsFriend(string myKey, string friendKey)
        {
            using (var db = GetConnection())
            {

                return db.Query<int>("select 1 from Friend where  MyKey=@myKey and FriendKey=@friendKey and deleted is null or Deleted <> 1 ", new { myKey, friendKey }).FirstOrDefault() > 0;
            }
        }

        public void Add(IUser user)
        {
            using (var db = GetConnection())
            {
                db.Execute("if not exists(select 1 from Account where UserKey = @key) " +
                           "insert into Account(UserKey, UserName, Password, OnlineStatus, RoleId) values (@key, @name, @password, @onlinestatus, @roleId);" +
                           "else " +
                           "update Account set UserName = @name, deleted = null,OnlineStatus=@onlinestatus, Password=@password  where UserKey = @key;",
                    new
                    {
                        name = user.Name,
                        key = user.Key,
                        password = user.Password,
                        onlinestatus = true,
                        roleId=1

                    });
            }
        }

        public void Remove(string key)
        {
            using (var db = GetConnection())
            {
                db.Execute("update Account set deleted = 1 where UserKey = @key",
                    new
                    {
                        key
                    });
            }
        }


       public  IList<IUser> GetAllUserList()
       {
           using (var db = GetConnection())
           {
               return db.Query<User>("select UserKey as [Key], UserName as Name from Account ").ToList().Cast<IUser>().ToList();
           }
       }

        public IList<IUser> GetList()
        {
            using (var db = GetConnection())
            {
                return db.Query<User>("select UserKey as [Key], UserName as Name from Account " +
                                      "where OnlineStatus=1  and deleted is null or Deleted <> 1 ").ToList().Cast<IUser>().ToList();
            }
        }

        public IList<IUser> GetListForAdmin()
        {
            using (var db = GetConnection())
            {
                return db.Query<User>(
                    "select a.UserKey as [Key], a.UserName as Name, a.Deleted, a.Created, a.CountOfNotSafeMessage, a.OnlineStatus, a.IpAdress, r.Rolename   from Account a  INNER JOIN Role r ON a.RoleId = r.Id ").ToList().Cast<IUser>().ToList();
            }
        }

        public IList<IUser> GetFriendList(string key)
        {
            using (var db = GetConnection())
            {
                return db.Query<User>("select a.UserKey as [Key], a.UserName as Name from Account a  INNER JOIN Friend s ON s.FriendKey = a.UserKey " +
                                      "where s.MyKey=@Key and s.deleted  is null or s.Deleted <> 1 and a.deleted is null or a.Deleted <> 1 ", new { Key = key }).ToList().Cast<IUser>().ToList();
            }
        }

        public IUser GetUserInfForAdmin(string key)
        {
            using (var db = GetConnection())
            {
                return
                    db.Query<User>(
                        "select a.UserKey as [Key], a.UserName as Name, a.Deleted, a.Created, a.CountOfNotSafeMessage, a.OnlineStatus, a.IpAdress , r.Rolename, r.Id as[RoleId]  from Account a  INNER JOIN Role r ON a.RoleId = r.Id " +
                        "where UserKey=@key ", new
                    {
                        key
                    }).FirstOrDefault();
            }
        }

        public IList<ITrend> GetTrendList()
        {

            using (var db = GetConnection())
            {
                return db.Query<Trend>("select Id as [Key], Name, Description, NumberOfReferences, TrendDate from Trend ").ToList().Cast<ITrend>().ToList();
            }
        }

        IDictionary<string, DateTime> _floodList = new Dictionary<string, DateTime>();
        public bool CheckFlooder(string key)
        {
            if (!_floodList.ContainsKey(key) || _floodList[key] < DateTime.Now.AddMinutes(-10))
                using (var db = GetConnection())
                {
                    int result = db.Query<int>("select count(1) from Chat where SenderKey = @key and SendDate > @date",
                        new
                        {
                            @key,
                            date = DateTime.Now.AddSeconds(-30)
                        }
                        ).FirstOrDefault();
                    if (result > 15 && !_floodList.ContainsKey(key))
                        _floodList.Add(key, DateTime.Now);
                    else if (result < 15 && _floodList.ContainsKey(key))
                        _floodList.Remove(key);
                    return result > 15;
                }
            else
                return true;
        }
        public void SaveMessage(ChatMessage message)
        {
            using (var db = GetConnection())
            {
                db.Execute(
                    "insert into Chat(RoomKey, SenderKey, SenderName, Content, SendDate, NotSafe) values" +
                    "(@RoomKey, @SenderKey, @SenderName, @Content, @SendDate, @NotSafe);",
                    new
                    {
                        message.RoomKey,
                        message.SenderKey,
                        message.SenderName,
                        message.Content,
                        SendDate = message.Date,
                        NotSafe=message.NotSafe
                    });
            }
        }

        public void SaveRoomInfo(RoomModel model)
        {
            using (var db = GetConnection())
            {
                db.Execute(
                    "insert into RoomInfo(RoomKey, MyPublicKey, MyKey, MyName, CalleePublicKey, CalleeKey, CalleeName, StartDate) " +
                    "values (@RoomKey, @MyPublicKey, @MyKey, @MyName, @CalleePublicKey, @CalleeKey, @CalleeName, @StartDate)",
                    new
                    {
                        model.RoomKey,
                        model.MyPublicKey,
                        model.MyKey,
                        model.MyName,
                        model.CalleePublicKey,
                        model.CalleeKey,
                        model.CalleeName,
                        StartDate = DateTime.Now
                    });
            }
        }
        public RoomModel GetRoomInfo(string key)
        {
            using (var db = GetConnection())
            {
                return db.Query<RoomModel>(
                    "select * from RoomInfo where RoomKey = @key",
                    new { key }).FirstOrDefault();
            }
        }
        public void CloseRoom(string chatKey)
        {
            using (var db = GetConnection())
            {
                db.Execute(
                    "update RoomInfo set EndDate = @endDate where RoomKey = @chatKey",
                    new
                    {
                        chatKey,
                        endDate = DateTime.Now
                    });
            }
        }


        public IList<ChatMessage> GetChatHistory(string roomKey)
        {
            using (var db = GetConnection())
            {
                return db.Query<ChatMessage>("select top 50 *, SendDate as Date from Chat where RoomKey = @roomKey " +
                                             "order by SendDate desc", new { roomKey }).ToList();
            }
        }

        public void CloseRoom(string roomKey, string key, VideoState state)
        {
            using (var db = GetConnection())
            {
                db.Execute("if exists(select 1 from RoomInfo where RoomKey = @roomKey and MyPublicKey = @key) " +
                           "update RoomInfo set MyVideoState = @videoState where RoomKey = @roomKey and MyPublicKey = @key " +
                           "else if exists(select 1 from RoomInfo where RoomKey = @roomKey and CalleePublicKey = @key) " +
                           "update RoomInfo set CalleeVideoState = @videoState where RoomKey = @roomKey and CalleePublicKey = @key",
                           new { roomKey, key, videoState = (int)state });
            }
        }

        public void SaveRoomState(string roomKey, string key, bool isCam, bool isCamOn, bool isMic, bool isMicOn)
        {
            using (var db = GetConnection())
            {
                var result = db.Execute("if exists(select 1 from RoomInfo where RoomKey = @roomKey and MyKey = @key) " +
                           "update RoomInfo set MyCam = @isCam, MyCamOn = @isCamOn, MyMic = @isMic, MyMicOn = @isMicOn " +
                           "where RoomKey = @roomKey and MyKey = @key else " +
                           "if exists(select 1 from RoomInfo where RoomKey = @roomKey and CalleeKey = @key) " +
                           "update RoomInfo set CalleeCam = @isCam, CalleeCamOn = @isCamOn, CalleeMic = @isMic, CalleeMicOn = @isMicOn " +
                           "where RoomKey = @roomKey and CalleeKey = @key",
                           new
                           {
                               roomKey,
                               key,
                               isCam = isCam ? 1 : 0,
                               isCamOn = isCamOn ? 1 : 0,
                               isMic = isMic ? 1 : 0,
                               isMicOn = isMicOn ? 1 : 0
                           }) > 0;
            }
        }


        public bool NameExists(string name)
        {
            using (var db = GetConnection())
            {
                return db.Query<int>("select 1 from Account where UserName=@name ", new { name }).FirstOrDefault() > 0;
            }
        }

        public void AddFriend(string mykey, string friendkey, string myname, string friendname)
        {
            using (var db = GetConnection())
            {
                db.Execute("if not exists(select 1 from Friend where FriendName = @FriendName and Myname=@MyName) " +
                           "insert into Friend(MyKey, FriendKey, MyName, FriendName) values (@MyKey, @FriendKey, @MyName, @FriendName);",
                    new
                    {
                        MyKey = mykey,
                        FriendKey = friendkey,
                        MyName = myname,
                        FriendName = friendname
                    });
            }
        }

        public void RemoveFriend(string mykey, string friendkey)
        {
            using (var db = GetConnection())
            {
                db.Execute("update Friend set deleted = 1 where MyKey = @Mykey and FriendKey=@Friendkey",
                    new
                    {
                        Mykey = mykey,
                        Friendkey = friendkey
                    });
            }
        }


        public void SetOnlineStatus(string key)
        {
            using (var db = GetConnection())
            {
                db.Execute("update Account set OnlineStatus = 1 where UserKey = @key",
                    new
                    {
                        key
                    });
            }
        }
        public void SetCountNotSafe(string key)
        {
            using (var db = GetConnection())
            {
                db.Execute("update Account set CountOfNotSafeMessage = CountOfNotSafeMessage+1 where UserKey = @key",
                    new
                    {
                        key
                    });
            }
        }

        public void SetOfOnlineStatus(string key)
        {
            using (var db = GetConnection())
            {
                db.Execute("update Account set OnlineStatus = 0 where UserKey = @key",
                    new
                    {
                        key
                    });
            }
        }

        public void EditUserProfile(User user)
        {
            using (var db = GetConnection())
            {
                db.Execute("update Account set UserName = @name, deleted = @deleted, CountOfNotSafeMessage=@countOfNotSafeMessage  where UserKey = @key;",
                    new
                    {
                        name = user.Name,
                        key = user.Key,
                        deleted = user.Deleted,
                        countOfNotSafeMessage = user.CountOfNotSafeMessage

                    });
            }
            
        }

        public List<string> GetUsersIp()
        {
            using (var db = GetConnection())
            {
                return db.Query<string>("select IpAdress from Account ").Distinct().ToList();
            }
        }

        public string GetUsersIp(string key)
        {
            using (var db = GetConnection())
            {
                return db.Query<string>("select IpAdress from Account where UserKey=@userKey",
                                        new
                                            {
                                                userKey = key
                                            }
                    ).FirstOrDefault();
            }
        }
        public void AddKeyWord(KeyWord model)
        {
            using (var db = GetConnection())
            {
                db.Execute("if not exists(select 1 from KeyWord where Name = @name) " +
                           "insert into KeyWord( Name, Description, ModeratorKey, CreatedDate) values (@name, @description, @moderatorKey,@createdDate);" +
                           "else " +
                           "update KeyWord set Name = @name, Description =  @description,ModeratorKey=@moderatorKey,CreatedDate=@createdDate where Id = @key;",
                    new
                    {
                        key=model.Key,
                        name = model.Name,
                        description = model.Description,
                        moderatorKey = model.ModeratorKey,
                        createdDate = DateTime.Now

                    });
            }
        }

        public void EditKeyWord(KeyWord model)
        {
            using (var db = GetConnection())
            {
                db.Execute("update KeyWord set Name = @name, Description =  @description,ModeratorKey= @moderatorKey,CreatedDate=@createdDate where Id = @key;",
                    new
                    {
                        key = model.Key,
                        name = model.Name,
                        description = model.Description,
                        moderatorKey = model.ModeratorKey,
                        createdDate = DateTime.Now

                    });
            }
        }

        public void DeleteKeyWord(int id)
        {
            using (var db = GetConnection())
            {
                db.Execute("DELETE FROM KeyWord WHERE  Id = @key",
                    new
                    {
                        key=id
                    });
            }
        }

        public IList<IKeyword> GetKeyWordList()
        {
            using (var db = GetConnection())
            {
                return db.Query<KeyWord>("select Id as [Key], Name, Description from KeyWord ").ToList().Cast<IKeyword>().ToList();
            }
        }

        public IKeyword GetKeyById(int id)
        {
            using (var db = GetConnection())
            {
                var list = GetKeyWordList();

                return list.FirstOrDefault(x => x.Key == id);
            }
        }

        public string GetRoleForUser(string key)
        {
            using (var db = GetConnection())
            {

                return db.Query<string>(
                        "select a.Rolename  from Role a  INNER JOIN Account s ON s.RoleId = a.Id " +
                        "where s.Userkey=@Key ",
                        new {Key = key}).FirstOrDefault();
                 
            }
        }
        public string GetRoleForCred(string name, string password)
        {
            using (var db = GetConnection())
            {

                return db.Query<string>(
                        "select a.Rolename  from Role a  INNER JOIN Account s ON s.RoleId = a.Id " +
                        "where s.UserName=@name and s.Password=@pass ",
                        new { name = name, pass=password }).FirstOrDefault();

            }
        }

        public int GetMaxIdForkey()
        {
            using (var db = GetConnection())
            {

                return db.Query<int>(
                    "select max(Id) from KeyWord").FirstOrDefault();

            }
        }


    }
}