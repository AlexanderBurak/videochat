﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;

namespace VideoChat.Models
{
    public class User : IUser
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool OnlineStatus {get;set; }
        public User() { }
        public bool Deleted { get; set; }
        public int CountOfNotSafeMessage { get; set; }
        public DateTime Created { get; set; }
        public User(string key, string name, string password)
        {
            Key = key;
            Name = name;
            Password = password;
        }
        public string UserIp { get; set; }
        public string RoleName { get; set; }
        public int RoleId { get; set; }
       
    }

    public class Trend : ITrend
    {
        public int Key { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int NumberOfReferences { get; set; }
    }
    public class KeyWord: IKeyword
    {
      public  int Key { get; set; }
      public string Name { get; set; }
      public string Description { get; set; }
      public string ModeratorKey { get; set; }
      public DateTime CreatedDate { get; set; }
    }

    public class Data
    {
        private readonly IKernel _ninjectKernel;

        public Data()
        {
            _ninjectKernel = new StandardKernel();
            AddBindings();
        }

        private void AddBindings()
        {
            _ninjectKernel.Bind<IUser>().To<User>();
            _ninjectKernel.Bind<IUsersStore>().To<DataBaseUsersStore>();
        }


        public IUsersStore GetUsersStore()
        {
            return _ninjectKernel.Get<IUsersStore>();
        }
        

    }
}