﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Expat.Bayesian;
using SignalR;
using SignalR.Hubs;
using VideoChat.Managers;
using System.Configuration;

namespace VideoChat.Models
{
    public class ChatMessage
    {
        public string RoomKey { get; set; }
        public string SenderKey { get; set; }
        public string SenderName { get; set; }
        public string Content { get; set; }

        private DateTime _date { get; set; }
        public DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                _formattedDate = _date.ToLongDateString() + " " + _date.ToLongTimeString();
            }
        }
        private string _formattedDate;
        public string FormattedDate
        {
            get { return _formattedDate; }
        }

        public bool NotSafe { get; set; }
    }

    public class RoomModel
    {
        public string RoomKey { get; set; }

        public string MyPublicKey { get; set; }
        public string MyKey { get; set; }
        public string MyName { get; set; }

        public string CalleePublicKey { get; set; }
        public string CalleeKey { get; set; }
        public string CalleeName { get; set; }
    }
    
    public class Chat : Hub, IDisconnect, IConnected
    {
        private IUsersStore _store;
        private IUsersStore Store
        {
            get
            {
                if (_store == null)
                {
                    var data = new Data();
                    _store = data.GetUsersStore();
                }
                return _store;
            }
        }

        // Обработка отключения от сервера
        public Task Disconnect()
        {
            // Удаляем юзера из хранилища
            Store.SetOfOnlineStatus(Context.ConnectionId);
            // Обновляем список изеров
            UpdateUsers();
            return Clients.leave(Context.ConnectionId, DateTime.Now.ToString());
        }


        // Обработка подключения к серверу
        public Task Connect()
        {
            UpdateUsers();
            return Clients.joined(Context.ConnectionId, DateTime.Now.ToString());
        }

        // Обработка переподключения к серверу
        public Task Reconnect(IEnumerable<string> groups)
        {
            UpdateUsers();
            return Clients.rejoined(Context.ConnectionId, DateTime.Now.ToString());
        }


        /// <summary>
        /// Метод обновления списка пользователей онлайн на клиентах
        /// </summary>
        public void UpdateUsers()
        {
            var userList = Store.GetList();
            var friendList = Store.GetFriendList(Context.ConnectionId);
            Clients[C.MAIN_CHAT_GROUP].OnUpdateUsers(userList);
            Clients[C.MAIN_CHAT_GROUP].OnUpdateFriends(friendList);
        }


        // вызывается клиентом для отправки сообщения
        public void Send(ChatMessage message)
        {
            SpamFilter _filter;
            Corpus bad = new Corpus();
            Corpus good = new Corpus();
            bad.LoadFromFile(ConfigurationManager.AppSettings["SpamFilePath"]);
            good.LoadFromFile(ConfigurationManager.AppSettings["AntiSpamFilePath"]);
            // баесовская вероятность спама
            double buesScore = 0;
            _filter = new SpamFilter();
            _filter.Load(good, bad);

            PointsSystem ps = new PointsSystem();
            // Что-то делаем только если сообщение не пустое
            if (message.Content.Length > 0)
            {
                if (!Store.CheckFlooder(Context.ConnectionId))
                {
                    //проставляем дату отправки
                    message.Date = DateTime.Now;
                    // идентификатор отправителя
                    message.SenderKey = Context.ConnectionId;
                    // экранируем пришедший текст во избежание хулиганства
                    message.Content = HttpUtility.HtmlEncode(message.Content);
                    message.SenderName = HttpUtility.HtmlEncode(message.SenderName);
                    // Оповещаем клиентов о новом сообщении


                    bool notSafeFlag = false;
                    string lang;
                    bool changes;
                    string content = "";
                    string re = "[\\w\\@]+";
                    Regex rx = new Regex(re, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                    Match m = rx.Match(message.Content);
                    while (m.Success)
                    {
                        string keyword= ps.CheckWord(m.ToString(), out lang, out changes);
                        content += " " + keyword;

                        if (!notSafeFlag)
                        {
                            notSafeFlag = ps.CheckInDictionary(keyword);
                        }
                        m = m.NextMatch();
                        
                    }

                    message.Content = content;
                    message.NotSafe = notSafeFlag;
                    buesScore = _filter.Test(message.Content);

                    if (notSafeFlag || buesScore > Convert.ToDouble(ConfigurationManager.AppSettings["SpamProbability"]))
                    {
                        Store.SetCountNotSafe(message.SenderKey);
                    }
                    Store.SaveMessage(message);
                    Clients[message.RoomKey].OnSend(message);
                }
                else
                {
                    message.Date = DateTime.Now;
                    message.SenderKey = Context.ConnectionId;
                    message.Content = HttpUtility.HtmlEncode(message.Content);
                    message.SenderName = HttpUtility.HtmlEncode(message.SenderName);
                    // Оповещаем флудера о новом сообщении
                    Clients[Context.ConnectionId].OnSend(message);
                }
            }
        }

        // вызывается клиентом для подключения к комнате.
        public void JoinRoom(string roomKey, string userName, string password)
        {
            // Сохраняем описание пользователя только если он в основном чате
            if (roomKey == C.MAIN_CHAT_GROUP)
            {
                if (Store.NameExists(HttpUtility.HtmlEncode(userName)))
                {
                    if (Store.UserExists(HttpUtility.HtmlEncode(userName),
                                     HttpUtility.HtmlEncode(password)))
                    {
                        Store.SetOnlineStatus(Context.ConnectionId);
                    }
                }
                else
                {
                    if (!Store.NameExists(userName))
                    {
                        Store.Add(new User(Context.ConnectionId, HttpUtility.HtmlEncode(userName), password));
                    }
                }
            }

            // Возвращаем юзеру его id
            Clients[Context.ConnectionId].OnJoinRoom(Context.ConnectionId);
            // Добавляем пользователя в комнату
            Groups.Add(Context.ConnectionId, roomKey);
            // Опопвещаем юзеров о изменении списка пользователей
            if (roomKey == C.MAIN_CHAT_GROUP)
                UpdateUsers();
        }


        /// Метод смены имени (логина)
        public void Rename(string newName, string password)
        {
            Store.Remove(Context.ConnectionId);
            Store.Add(new User(Context.ConnectionId, HttpUtility.HtmlEncode(newName), password));
            UpdateUsers();
        }

        /// Метод звонка (набор номера)
        public void Call(string recieverKey, string senderKey, string senderName)
        {
            Clients[recieverKey].OnCall(senderKey, HttpUtility.HtmlEncode(senderName));
        }

        /// Метод отклонения звонка
        public void RejectCall(string senderKey, string recieverKey, string recieverName)
        {
            Clients[senderKey].OnRejectCall(recieverKey, HttpUtility.HtmlEncode(recieverName));
        }

        /// Принятие звонка
        public void AcceptCall(string calleePulicKey, string calleeName, string myName)
        {
            string myKey = Guid.NewGuid().ToString().Replace("-", "");
            string calleeKey = Guid.NewGuid().ToString().Replace("-", "");
            string roomKey = Guid.NewGuid().ToString().Replace("-", "");

            var model = new RoomModel
            {
                MyPublicKey = Context.ConnectionId,
                MyKey = myKey,
                MyName = myName,
                CalleePublicKey = calleePulicKey,
                CalleeKey = calleeKey,
                CalleeName = calleeName,
                RoomKey = roomKey
            };

            // Сохраняем информацию о начинающемся сеансе
            Store.SaveRoomInfo(model);
            // Рассылаем уведомления
            Clients[calleePulicKey].OnAcceptCall(false, roomKey);
            Clients[Context.ConnectionId].OnAcceptCall(true, roomKey);
        }
    }

    public class ChatConnectionIdGenerator : IConnectionIdGenerator
    {
        public string GenerateConnectionId(IRequest request)
        {
            if (request.Cookies["key"] != null)
            {
                var cook = request.Cookies["key"];

                if (!String.IsNullOrEmpty(cook.Value))
                {
                    return cook.Value;
                }
            }

            throw new InvalidOperationException("No cookie");
        }
    }
}