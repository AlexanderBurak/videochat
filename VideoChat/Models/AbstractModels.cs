﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoChat.Models
{
    public static class C
    {
        public const string MAIN_CHAT_GROUP = "MAIN";
    }

    public enum VideoState
    {
        NOT_WORK = 1,
        SOUND = 2,
        VIDEO = 3,
        SOUND_VIDEO = 4
    }

    public interface IUsersStore
    {
        bool Exists(string key, string name, string password);
        bool NameExists(string name);
        bool UserExists(string name, string password);
        bool CheckIsFriend(string myKey, string friendKey);
        string GetKeyForExistUser(string name, string password);
        void Add(IUser user);
        void Remove(string key);
        void SetOnlineStatus(string key);
        void SetOfOnlineStatus(string key);
        IList<IUser> GetList();
        IList<IUser> GetAllUserList();
        IList<ITrend> GetTrendList();
        IList<IUser> GetListForAdmin();
        IUser GetUserInfForAdmin(string key);
        void EditUserProfile(User model);
        List<string> GetUsersIp();
        string GetUsersIp(string key);
        string GetRoleForUser(string key);
        IKeyword GetKeyById(int Id);
        void SetCountNotSafe(string key);
        void AddFriend(string mykey, string friendkey, string myname, string friendname);
        void RemoveFriend(string mykey, string friendkey);
        IList<IUser> GetFriendList(string key);

        bool CheckFlooder(string key);
        void SaveMessage(ChatMessage message);
        void SaveRoomInfo(RoomModel model);
        RoomModel GetRoomInfo(string key);
        void CloseRoom(string chatKey);

        IList<ChatMessage> GetChatHistory(string roomKey);
        void CloseRoom(string roomKey, string userKey, VideoState state);
        void SaveRoomState(string roomKey, string key, bool isCam, bool isCamOn, bool isMic, bool isMicOn);

        void AddKeyWord(KeyWord model);
        void EditKeyWord(KeyWord model);
        void DeleteKeyWord(int id);
        IList<IKeyword> GetKeyWordList();
        string GetRoleForCred(string name, string password);
        int GetMaxIdForkey();
    }

    public interface IUser
    {
        string Key { get; set; }
        string Name { get; set; }
        string Password { get; set; }
        bool OnlineStatus { get; set; }
        bool Deleted { get; set; }
        int CountOfNotSafeMessage { get; set; }
        DateTime Created { get; set; }
        string UserIp { get; set; }
        string RoleName { get; set; }
        int RoleId { get; set; }
    }

    public interface ITrend
    {
        int Key { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        int NumberOfReferences { get; set; }

    }

    public interface IKeyword
    {
        int Key { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        string ModeratorKey { get; set; }
        DateTime CreatedDate { get; set; }
    }
}